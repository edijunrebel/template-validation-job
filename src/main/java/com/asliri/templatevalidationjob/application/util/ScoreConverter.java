package com.asliri.templatevalidationjob.application.util;

import java.text.DecimalFormat;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ScoreConverter {
	private static final float MEDIUM_SCORE = 40f;
	private static final float HIGH_SCORE = 99f;

	private static final float BASE_PERCENT = 0f;
	private static final float LOW_PERCENT = 75f;
	private static final float MEDIUM_PERCENT = 97f;
	private static final float HIGH_PERCENT = 98f;

	public static float percentage(int score) {
		log.debug("Start converting score...");
		
		if (score < MEDIUM_SCORE) {
			float result = getPercentLow(score);
			log.debug("Original Score : {} ====> converted percentage : {}", score, result);
			return result;
		}
		if (score > MEDIUM_SCORE && score <= HIGH_SCORE) {
			float result = getPercentMedium(score);
			log.debug("Original Score : {} ====> converted percentage : {}", score, result);
			return result;
		}
		if (score > HIGH_SCORE) {
			float result = HIGH_PERCENT;
			log.debug("Original Score : {} ====> converted percentage : {}", score, result);
			return result;
		}
		
		float result = BASE_PERCENT;
		log.debug("Original Score : {} ====> converted percentage : {}", score, result);
		return result;
	}

	private static float getPercentLow(int score) {
		float result = ((score / (float) MEDIUM_SCORE) * LOW_PERCENT);
		return format(result);
	}

	private static float getPercentMedium(int score) {
		float add = ((score - MEDIUM_SCORE) * (MEDIUM_PERCENT - LOW_PERCENT)) / (HIGH_SCORE - MEDIUM_SCORE);
		float result =  LOW_PERCENT + add;
		return format(result);
	}

	public static float format(float value) {
		if (value > 0) {
			DecimalFormat df = new DecimalFormat();
			df.setMaximumFractionDigits(1);
			return Float.valueOf(df.format(value));
		}
		return 0f;
	}
}
