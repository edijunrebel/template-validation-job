package com.asliri.templatevalidationjob.application.util;

import java.util.Base64;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ImageUtils {

    public static boolean isFMR(String base64) {
        byte[] bytes = null;
		try {
			bytes = Base64.getDecoder().decode(base64);
		} catch (IllegalArgumentException e) {
			log.info("file is not fmr");
		}
        String[] images = new String(bytes).split(" ");
        if (images.length > 0 && "FMR".equalsIgnoreCase(images[0].trim())) {
            return true;
        }
        return false;
    }
}
