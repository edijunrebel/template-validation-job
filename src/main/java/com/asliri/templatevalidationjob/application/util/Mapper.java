package com.asliri.templatevalidationjob.application.util;

import java.io.IOException;
import java.util.Optional;

import org.modelmapper.ModelMapper;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

public class Mapper {

	public static <T> T parse(Class<T> clazz, String jsonString) {
		return Optional.ofNullable(jsonString).map(json -> {
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
				return mapper.readValue(json, clazz);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}).orElse(null);
	}
	
	public static <T> T parseTypeRef(TypeReference typeRef, String jsonString) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return (T) mapper.readValue(jsonString, typeRef);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String toString(Object object) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <T> T toObject(Class<T> clazz, Object object) {
		return Optional.ofNullable(object).map(obj -> {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
			return mapper.convertValue(obj, clazz);
		}).orElse(null);
	}
	
	public static <T> T mapObject(Object object, Class<T> clazz) {
		return Optional.ofNullable(object).map(obj -> {
			ModelMapper modelMapper = new ModelMapper();
			modelMapper.getConfiguration().setAmbiguityIgnored(true);
			return modelMapper.map(obj, clazz);
		}).orElse(null);
	}
}
