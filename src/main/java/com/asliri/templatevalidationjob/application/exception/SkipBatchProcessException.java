package com.asliri.templatevalidationjob.application.exception;

public class SkipBatchProcessException extends RuntimeException {
	private static final long serialVersionUID = 4112756003681927593L;

	public SkipBatchProcessException() {
		super("Error accoured when processing batch process");
	}

}
