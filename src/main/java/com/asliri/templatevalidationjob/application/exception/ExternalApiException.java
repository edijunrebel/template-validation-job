package com.asliri.templatevalidationjob.application.exception;

import org.springframework.http.HttpStatus;

public class ExternalApiException extends RuntimeException {
	private static final long serialVersionUID = 4112756003681927593L;

	public ExternalApiException(HttpStatus status) {
		super(status.name());
	}

}
