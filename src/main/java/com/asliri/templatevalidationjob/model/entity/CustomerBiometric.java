package com.asliri.templatevalidationjob.model.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Data
@Entity
@Table(name = "CUSTOMER_BIOMETRIC")
public class CustomerBiometric {

	@Id
	@Column(name = "CIF", nullable = false, updatable = false)
	private String cif;

	@Column(name = "CRN")
	private String crn;

	@Column(name = "DOB", nullable = false)
	private Date dob;

	@Column(name = "BRANCH_CODE", nullable = false, updatable = false)
	private String branchCode;

	@Column(name = "FACE")
	private String face;

	@Column(name = "FP_1")
	private String fp1;

	@Column(name = "FP_2")
	private String fp2;

	@Column(name = "FP_1_INDEX")
	private Integer fp1Index;

	@Column(name = "FP_2_INDEX")
	private Integer fp2Index;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@CreationTimestamp
	@Column(name = "CREATED_DATE")
	private Timestamp createdDate;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@UpdateTimestamp
	@Column(name = "MODIFIED_DATE")
	private Timestamp modifiedDate;

}
