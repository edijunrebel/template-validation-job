package com.asliri.templatevalidationjob.model.repository;

import com.asliri.templatevalidationjob.model.repository.payload.FingerQualityRequest;
import com.asliri.templatevalidationjob.model.repository.payload.ScorePayload;

public interface FingerprintRepository {

	ScorePayload checkFingerQuality(FingerQualityRequest request);

}
