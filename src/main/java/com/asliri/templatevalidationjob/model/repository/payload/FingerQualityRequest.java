package com.asliri.templatevalidationjob.model.repository.payload;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FingerQualityRequest {

	private String fingerprint;

	private Integer index;

}
