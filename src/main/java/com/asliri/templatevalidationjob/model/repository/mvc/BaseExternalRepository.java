package com.asliri.templatevalidationjob.model.repository.mvc;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;

import com.asliri.templatevalidationjob.application.exception.ExternalApiException;
import com.asliri.templatevalidationjob.application.util.Mapper;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
public class BaseExternalRepository {
	
	protected WebClient webClient;
	
	StringBuilder sbLog = new StringBuilder();
	
	public BaseExternalRepository() {
		this.webClient = WebClient.builder()
				.filters(exchangeFilterFunctions -> {
					exchangeFilterFunctions.add(logRequest());
				}).build();
	}
	
	ExchangeFilterFunction logRequest() {
		return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
			if (log.isDebugEnabled()) {
				sbLog.append("Request to : " + clientRequest.method() + " " + clientRequest.url() + "\n");
				clientRequest.headers()
						.forEach((name, values) -> values.forEach(value -> sbLog.append(name + " : " + value + "\n")));
			}
			return Mono.just(clientRequest);
		});
	}
	
	protected String getBody(Object request, ClientResponse clientResponse) {
		sbLog.append("Request body : " + Mapper.toString(request));
		HttpStatus status = clientResponse.statusCode();
		sbLog.append("Response status : " + status + "\n");

		String body = clientResponse.bodyToMono(new ParameterizedTypeReference<String>() {
		}).block();
		sbLog.append("Response body : " + body);
		
		log.debug(sbLog.toString());

		if (status == HttpStatus.OK) {
			return body;
		} else {
			throw new ExternalApiException(clientResponse.statusCode());
		}
	}

}
