package com.asliri.templatevalidationjob.model.repository.payload;

import lombok.Data;

@Data
public class ScorePayload {

	private Integer score;

	private Float percent;

}
