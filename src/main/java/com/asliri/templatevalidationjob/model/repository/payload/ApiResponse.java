package com.asliri.templatevalidationjob.model.repository.payload;

public class ApiResponse<T> {

	private String message;

	private T data;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}