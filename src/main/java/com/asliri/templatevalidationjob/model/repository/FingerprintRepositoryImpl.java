package com.asliri.templatevalidationjob.model.repository;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;

import com.asliri.templatevalidationjob.application.exception.GeneralValidationException;
import com.asliri.templatevalidationjob.application.util.Mapper;
import com.asliri.templatevalidationjob.model.repository.mvc.BaseExternalRepository;
import com.asliri.templatevalidationjob.model.repository.payload.ApiResponse;
import com.asliri.templatevalidationjob.model.repository.payload.FingerQualityRequest;
import com.asliri.templatevalidationjob.model.repository.payload.ScorePayload;
import com.fasterxml.jackson.core.type.TypeReference;

import reactor.core.publisher.Mono;

@Repository
public class FingerprintRepositoryImpl extends BaseExternalRepository implements FingerprintRepository {

	@Value("${fingerprintquality.url}")
	private String fingerprintQualityUrl;

	@Override
	public ScorePayload checkFingerQuality(FingerQualityRequest request) {
		Mono<ClientResponse> monoClientResponse = webClient.post().uri(fingerprintQualityUrl)
				.body(BodyInserters.fromObject(request)).exchange();

		ClientResponse clientResponse = monoClientResponse.block();
		String body = getBody(request, clientResponse);
		TypeReference<ApiResponse<ScorePayload>> typeRef = new TypeReference<ApiResponse<ScorePayload>>() {
		};
		ApiResponse<ScorePayload> response = Mapper.parseTypeRef(typeRef, body);

		if (Objects.isNull(response.getMessage())) {
			return response.getData();
		} else {
			String message = response.getMessage().substring(response.getMessage().lastIndexOf(": ") + 2);
			throw new GeneralValidationException(message);
		}
	}

}
