package com.asliri.templatevalidationjob.module.validatefinger.batch;

import lombok.Data;

@Data
public class CustomerFinger {

	private String cif;

	private String fp1;

	private String fp2;
	
	private Integer fp1Index;

	private Integer fp2Index;
	
	private int fp1Score;
	
	private int fp2Score;

}
