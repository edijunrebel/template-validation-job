package com.asliri.templatevalidationjob.module.validatefinger.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CustomerFingerRowMapper implements RowMapper<CustomerFinger> {

	@Override
	public CustomerFinger mapRow(ResultSet rs, int rowNum) throws SQLException {
		CustomerFinger customerFinger = new CustomerFinger();
		customerFinger.setCif(rs.getString("CIF"));
		customerFinger.setFp1(rs.getString("FP_1"));
		customerFinger.setFp2(rs.getString("FP_2"));
		customerFinger.setFp1Index(rs.getInt("FP_1_INDEX"));
		customerFinger.setFp2Index(rs.getInt("FP_2_INDEX"));
		
		return customerFinger;
	}

}
