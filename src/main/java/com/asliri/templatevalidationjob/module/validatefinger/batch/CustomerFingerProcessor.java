package com.asliri.templatevalidationjob.module.validatefinger.batch;

import java.util.Objects;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.asliri.templatevalidationjob.application.util.ImageUtils;
import com.asliri.templatevalidationjob.application.util.Mapper;
import com.asliri.templatevalidationjob.application.util.ScoreConverter;
import com.asliri.templatevalidationjob.model.repository.FingerprintRepository;
import com.asliri.templatevalidationjob.model.repository.payload.FingerQualityRequest;
import com.asliri.templatevalidationjob.model.repository.payload.ScorePayload;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomerFingerProcessor implements ItemProcessor<CustomerFinger, CustomerFinger> {

	@Autowired
	private FingerprintRepository fingerprintRepository;

	@Override
	public CustomerFinger process(CustomerFinger item) throws Exception {
		log.info("Now processing for data of CIF : {}", item.getCif());
		int fp1Score = checkImageQuality(item.getFp1(), item.getFp1Index());
		item.setFp1Score(fp1Score);
		
		log.info("Validation result of index {} : {}", item.getFp1Index(), fp1Score);

		int fp2Score = checkImageQuality(item.getFp2(), item.getFp2Index());
		item.setFp2Score(fp2Score);
		
		log.info("Validation result of index {} : {}", item.getFp2Index(), fp2Score);
		
		log.info("Validation complete");
		return item;
	}

	private int checkImageQuality(String finger, int index) {
		log.info("Validating finger quality...");
		if (Objects.isNull(finger) || finger.isEmpty()) {
			log.info("Finger does not exist");
			return 0;
		}
		FingerQualityRequest qualityApiRequest = FingerQualityRequest.builder().fingerprint(finger).index(index)
				.build();
		
		log.info(Mapper.toString(qualityApiRequest));

		ScorePayload scorePayload = fingerprintRepository.checkFingerQuality(qualityApiRequest);
		Float percent = ScoreConverter.percentage(scorePayload.getScore());

		return Math.round(percent);
	}

}
