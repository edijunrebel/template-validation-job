package com.asliri.templatevalidationjob.module.validatefinger.batch;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.asliri.templatevalidationjob.application.exception.SkipBatchProcessException;


@Configuration
public class BatchConfiguration extends DefaultBatchConfigurer {
	
	@Autowired
	private JobBuilderFactory jobs;

	@Autowired
	private StepBuilderFactory steps;

	@Autowired
	public DataSource dataSource;
	
	private static final String QUERY_ALL_CUSTOMER = "SELECT CIF, FP_1, FP_2, FP_1_INDEX, FP_2_INDEX FROM dbo.CUSTOMER_BIOMETRIC";
	
	private static final String QUERY_UPDATE_RESULT = 
			"UPDATE dbo.CUSTOMER_BIOMETRIC SET "
			+ "FP_1_SCORE = :fp1Score, "
			+ "FP_2_SCORE = :fp2Score "
			+ "WHERE CIF = :cif";
	
	@Bean
	public ItemReader<CustomerFinger> databaseItemReader() {
		JdbcCursorItemReader<CustomerFinger> databaseReader = new JdbcCursorItemReader<>();

		databaseReader.setDataSource(dataSource);
		databaseReader.setSql(QUERY_ALL_CUSTOMER);
		databaseReader.setRowMapper(new CustomerFingerRowMapper());

		return databaseReader;
	}
	
	@Bean
	public ItemProcessor<CustomerFinger, CustomerFinger> customerFingerProcessor() {
		return new CustomerFingerProcessor();
	}
	
	@Bean
	public JdbcBatchItemWriter<CustomerFinger> dbCustomerWriter() {
		JdbcBatchItemWriter<CustomerFinger> dbCustomerWriter = new JdbcBatchItemWriter<CustomerFinger>();
		dbCustomerWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<CustomerFinger>());
		dbCustomerWriter
				.setSql(QUERY_UPDATE_RESULT);
		dbCustomerWriter.setDataSource(dataSource);
		return dbCustomerWriter;
	}
	
	@Bean
	public Step validateFingerStep() {
		return steps.get("validateFingerStep")
				.<CustomerFinger, CustomerFinger>chunk(1)
				.reader(databaseItemReader())
				.processor(customerFingerProcessor())
				.writer(dbCustomerWriter())
				.faultTolerant()
				.skipLimit(100)
				.skip(SkipBatchProcessException.class)
				.build();
	}
	
	@Bean(name = "validateFingerJob")
	public Job validateFingerJob(JobCompletionNotificationListener listener) {
		return jobs.get("validateFingerJob")
				.incrementer(new RunIdIncrementer())
				.listener(listener)
				.flow(validateFingerStep())
				.end()
				.build();
	}
	
	@Override
	public void setDataSource(DataSource dataSource) {
		// override to do not set datasource even if a datasource exist.
		// initialize will use a Map based JobRepository (instead of database)
	}
}
