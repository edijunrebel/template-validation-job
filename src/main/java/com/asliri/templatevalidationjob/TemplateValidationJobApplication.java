package com.asliri.templatevalidationjob;

import org.apache.logging.log4j.core.config.Configurator;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
@EnableBatchProcessing
public class TemplateValidationJobApplication implements CommandLineRunner {
	
	@Value("${app.log4j.path}")
	private String log4path;

	public static void main(String[] args) {
		SpringApplication.run(TemplateValidationJobApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		Configurator.initialize(null, log4path);
		log.info("initialize logger application");
	}

}
